package discount.weekend;

public interface DayOfWeekBehaviour {
    double calculatePrice();
}
