package domain;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.time.DayOfWeek;
import java.io.FileWriter;

public class Order {
    private int orderNr;
    private boolean isStudentOrder;
    private ArrayList<MovieTicket> tickets;

    public Order(int orderNr, boolean isStudentOrder) {
        this.orderNr = orderNr;
        this.isStudentOrder = isStudentOrder;
        tickets = new ArrayList<MovieTicket>();
    }

    public int getOrderNr() {
        return orderNr;
    }

    public void addSeatReservation(MovieTicket ticket) {
        tickets.add(ticket);
    }

    public void addSeatReservations(ArrayList<MovieTicket> tickets) {this.tickets.addAll(tickets); }

    public double calculatePrice() {
        double totalPrice = 0;
        boolean isStudent = this.isStudentOrder;
        boolean isGroup = this.tickets.size() > 5;
        boolean flipNext = false;

        for (MovieTicket ticket : tickets) {

            // Student Logic
            if (isStudent) {
                if (!flipNext) {
                    if (ticket.isPremiumTicket()) totalPrice += (ticket.getPrice() + 2);
                    else totalPrice += ticket.getPrice();
                    flipNext = true;
                } else {
                    flipNext = false;
                }
            }

            // Dow Logic
            else if (isWeekend(ticket)) {
                if (ticket.isPremiumTicket()) totalPrice += (ticket.getPrice() + 3);
                else totalPrice += ticket.getPrice();
            } else {
                if (!flipNext) {
                    if (ticket.isPremiumTicket()) totalPrice += (ticket.getPrice() + 3);
                    else totalPrice += ticket.getPrice();
                    flipNext = true;
                } else {
                    flipNext = false;
                }
            }
        }

        // More than 5 people and no students.
        if (!isStudent && isGroup) totalPrice = totalPrice * 0.9;

        return totalPrice;
    }

    private boolean isWeekend(MovieTicket ticket) {
        DayOfWeek dayOfWeek = ticket.movieScreening.dateAndTime.getDayOfWeek();
        return dayOfWeek == DayOfWeek.FRIDAY || dayOfWeek == DayOfWeek.SATURDAY || dayOfWeek == DayOfWeek.SUNDAY;
    }

    public void export(TicketExportFormat exportFormat) throws IOException {
        ArrayList<String> writeLst = new ArrayList<>();

        for (int i = 0; i < this.tickets.size(); i++) {
            writeLst.add(this.tickets.get(i).toString());
        }

        if (exportFormat == TicketExportFormat.PLAINTEXT) {
            FileWriter writer = new FileWriter("Order_ " + this.orderNr + ".txt");
            for (String str : writeLst) {
                writer.write(str + System.lineSeparator());
            }
            writer.close();
        } else {
            try (Writer writer = new FileWriter("Order_ " + this.orderNr + ".json")) {
                Gson gson = new GsonBuilder().create();
                gson.toJson(writeLst, writer);
            }
        }

        // Bases on the string respresentations of the tickets (toString), write
        // the ticket to a file with naming convention Order_<orderNr>.txt of
        // Order_<orderNr>.json
    }

    @Override
    public String toString() {
        return "domain.Order{" +
                "orderNr=" + orderNr +
                ", isStudentOrder=" + isStudentOrder +
                ", tickets=" + tickets +
                '}';
    }
}
