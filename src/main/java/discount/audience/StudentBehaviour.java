package discount.audience;

public interface StudentBehaviour {
    double calculatePrice();
}
