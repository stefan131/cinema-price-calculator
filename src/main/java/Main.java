import domain.Movie;
import domain.MovieScreening;
import domain.MovieTicket;
import domain.Order;

import java.time.LocalDateTime;

public class Main {

    public static void main(String[] args) {
        Order order = new Order(5, false);
        order.addSeatReservation(new MovieTicket(new MovieScreening(new Movie("That one movie"), LocalDateTime.now(), 10.00), true, 1, 2));
        order.addSeatReservation(new MovieTicket(new MovieScreening(new Movie("That one movie"), LocalDateTime.now(), 10.00), false, 1, 2));
        order.addSeatReservation(new MovieTicket(new MovieScreening(new Movie("That one movie"), LocalDateTime.now(), 10.00), true, 1, 2));
        order.addSeatReservation(new MovieTicket(new MovieScreening(new Movie("That one movie"), LocalDateTime.now(), 10.00), false, 1, 2));
        System.out.println(order.calculatePrice());
    }
}
