package discount.premium;

public interface PremiumBehaviour {
    double calculatePrice();
}
